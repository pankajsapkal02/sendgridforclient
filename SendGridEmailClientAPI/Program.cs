﻿using System;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

using System.Net.Http;
using System.IO;

namespace SendGridEmail
{
    public class Program
    {
        private readonly HttpClient httpClient;
        static void Main(string[] args)
        {
            string apiKey = "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";
            sendEmailUsingSendGrid(apiKey).Wait();
        }

        static async Task sendEmailUsingSendGrid(string apiKey)
        {
            try
            {


                var client = new SendGridClient(apiKey);
                //send an email message using the SendGrid Web API with a console application.  
                var msgs = new SendGridMessage()
                {
                    From = new EmailAddress("pankaj.sapkal@dev.figmd.com", "Pankaj Sapkal"),
                    //Template ID format of mail
                    TemplateId = "90e858b5-deec-4d94-a14d-da40516010a4",
                };
                //if you have multiple reciepents to send mail  
                msgs.AddTo(new EmailAddress("pankaj.sapkal@dev.figmd.com", "Pankaj Sapkal"));
                ///CC and BCC emails if you are passing.
                //if (!(string.IsNullOrEmpty(ccEmail)))
                //    msgs.AddCc(new SendGrid.Helpers.Mail.EmailAddress(ccEmail));
                //if (!(string.IsNullOrEmpty(bccEmail)))
                //    msgs.AddBcc(new SendGrid.Helpers.Mail.EmailAddress(bccEmail));

                ///If you are using any placeholder which need to replace with  your values.
                msgs.AddSubstitution("-NPI-", "NPI");
                msgs.AddSubstitution("-FirstName-", "FirstName");
                msgs.AddSubstitution("-LastName-", "LastName");
                msgs.AddSubstitution("-Code-", "ActivityID");
                msgs.AddSubstitution("-ActivityName-", "ActivityName");
                msgs.AddSubstitution("-PIID-", "PIID");
                msgs.AddSubstitution("-weekly_monthly_quarterly-", "Weekly_Monthly_Quarterly");

                var response = await client.SendEmailAsync(msgs);
                //logging your response and parameters if you want
                //File.AppendAllText(@"D:\SendGridTest\pankaj\test.txt", "Response:-  " + ", Status Code - " + response.StatusCode + ", response - " + response.ToString() + ", Message" + msgs.ToString());

                if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
                {
                    // See if we can read the response for more information, then log the error
                    var errorJson = response.ToString();
                    throw new Exception(
                        "SendGrid indicated failure! Code: {response.StatusCode}, reason: {errorJson}");
                }
                else
                {
                    //File.AppendAllText(@"D:\SendGridTest\pankaj\test.txt", "Else :- " + Convert.ToString(response.StatusCode));
                }
            }
            catch (Exception et)
            {
                // File.AppendAllText(@"D:\SendGridTest\pankaj\test.txt", "Exception InnerException:-  " + et.ToString() + toEmail + apiKey);
            }

        }

    }

}

